"use strict";

const jwt = require("jwt-simple");
const moment = require("moment");

const services = {}
const secreto = "todoListSecret";
services.createToken = function(user) {
    var payload = {
        sub: user.id,
        nombre: user.firstName,
        email: user.email,
        iat: moment().unix(),
        exp: moment().add(30, "days").unix
    };

    return jwt.encode(payload, secreto);
};

services.decodetoken = function(token) {
    return jwt.decode(token, secreto);
};


module.exports = services