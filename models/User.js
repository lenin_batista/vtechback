const { DataTypes } = require('sequelize')
const sequelize = require('../bd/connection')


const User = sequelize.define('user', {
    firstName: {
        type: DataTypes.STRING(50)
    },
    lastName: {
        type: DataTypes.STRING(50)
    },
    email: {
        type: DataTypes.STRING(50),
        allowNull: false
    },
    fgActive: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    passWord: {
        type: DataTypes.STRING(100)
    }

})

User.sync()
    // User.sync({ force: true })

module.exports = User