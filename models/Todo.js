const { DataTypes } = require('sequelize')
const sequelize = require('../bd/connection')

const TodoList = sequelize.define('todolist', {
    name: {
        type: DataTypes.STRING(150)
    },
    state: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    fgActive: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    }

})
TodoList.sync()
module.exports = TodoList