const token = require("../services/services");

const verifyToken = (req, res, next) => {
    try {
        const bearerHeader = req.headers.authorization;

        if (!bearerHeader) {
            return res.status(400).send({ msg: "invalid token" })
        }


        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];
        console.log(bearerHeader, bearerToken)
        const tokenDecode = token.decodetoken(bearerToken);

        req.body.token = tokenDecode;

        next();
    } catch (error) {
        return res.status(400).send({ msg: "invalid token" })
    }
};

module.exports = verifyToken;