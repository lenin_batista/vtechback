const todoList = require('../controller/todoList')
const verifyToken = require("../lib/verifyToken");
const router = require('express').Router()

router.get("/", verifyToken, todoList.getAll)
router.get("/:id", verifyToken, todoList.getById)
router.post("/", verifyToken, todoList.createTask)
router.put("/:id", verifyToken, todoList.doneTask)
router.delete("/:id", verifyToken, todoList.deleteTask)


module.exports = router