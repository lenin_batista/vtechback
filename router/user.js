const router = require('express').Router()
const User = require('../controller/user')
const verifyToken = require("../lib/verifyToken");


router.post("/", User.createUser)
router.post("/auth/", User.userAuth)

module.exports = router