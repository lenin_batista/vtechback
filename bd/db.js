const mysql = require('mysql2/promise');

const dbName = 'todolist'
const host = 'localhost'
const password = '12345678'
const user = 'root'
const port = 3306

const createDb = {
    init: () => {

        try {
            const connection = await mysql.createConnection({ host, port, user, password });
            await connection.query(`CREATE DATABASE IF NOT EXISTS \`${dbName}\`;`);
            return true
        } catch (err) {
            console.log(err)
            return false
        }
    }
}




module.exports = createDb