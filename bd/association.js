const TodoList = require("../models/Todo");
const User = require("../models/User");


TodoList.belongsTo(User);
User.hasMany(TodoList);