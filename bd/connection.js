const { Sequelize } = require('sequelize');
const mysql = require('mysql2/promise');

const dbName = 'todolist'
const host = 'localhost'
const password = '12345678'
const user = 'root'

const sequelize = new Sequelize(dbName, user, password, {
    host,
    dialect: 'mysql',
    sync: { force: true },

});

// sequelize.sync({ force: true })

module.exports = sequelize