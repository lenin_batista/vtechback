const bcrypt = require("bcrypt");
var services = require("../services/services");
const User = require("../models/User");

const userController = {
    getAll: async(req, res) => {
        const userData = await User.findAll();
        res.status(200).send(userData);
    },

    getById: async(req, res) => {
        const { id } = req.params;
        const userData = await User.findByPk(id);
        res.status(200).send(userData);
    },

    createUser: async(req, res) => {
        try {
            const { body } = req;
            const saltRounds = 10;
            bcrypt.genSalt(saltRounds, function(err, salt) {
                bcrypt.hash(body.passWord, salt, function(err, hash) {
                    console.log(hash);
                    body.passWord = hash;
                    const userData = new User(body);
                    userData.save().then((data) => {
                        res.status(200).send(data);
                    });
                });
            });
        } catch (err) {
            res.status(400).rend({ msg: `Bad Request` });
        }
    },

    userAuth: async(req, res) => {
        const { email, passWord } = req.body;
        const userData = await User.findOne({
            attributes: ["id", "firstName", "lastName", "email", "passWord"],
            where: {
                email
            },
        });


        if (!userData) return res.status(404).send({ msg: "El usuario no existe, intente nuevamente" })
        bcrypt.compare(passWord, userData.passWord, function(err, result) {
            // result == true
            if (result) {

                const token = services.createToken(userData);
                const response = {...userData.dataValues, token };
                delete response.passWord
                res.status(200).send({ response });
            } else {
                res.status(200).send({ msg: "Invalid Pass" });
            }
        });


    },

    deleteUser: async(req, res) => {
        try {
            const { id } = req.params;
            const userData = await User.findByPk(id);
            const fgActive = { fgActive: false };
            if (!userData) res.status(200).send({ msg: "User not found" });

            userData.update(fgActive);
            res.status(200).send(userData);
        } catch (err) {
            res.status(400).rend({ msg: `Bad Request` });
        }
    },
};

module.exports = userController;