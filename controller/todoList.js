const TodoList = require("../models/Todo")



const todoList = {

    getAll: async(req, res) => {
        const taskData = await TodoList.findAll({
            attributes: ["id", "name", "state", "fgActive"],
            where: {
                fgActive: true
            }
        })
        res.status(200).send(taskData)
    },

    getById: async(req, res) => {

        const { id } = req.params
        const taskData = await TodoList.findByPk(id)
        res.status(200).send(taskData)
    },

    createTask: async(req, res) => {
        try {
            const { body } = req
            const taskData = new TodoList(body)
            await taskData.save()
            res.status(200).send(taskData)
        } catch (err) {
            res.status(400).send({ msg: `Bad Request` })
        }

    },

    deleteTask: async(req, res) => {
        try {
            console.log("[deleteTask]", req.params)
            const { id } = req.params
            const taskData = await TodoList.findByPk(id)
            const fgActive = { fgActive: false }
            if (!taskData)
                res.status(200).send({ msg: 'Task not found' })

            taskData.update(fgActive)
            res.status(200).send(taskData)
        } catch (err) {

            res.status(400).send({ msg: `Bad Request` })
        }
    },

    doneTask: async(req, res) => {
        try {
            const { id } = req.params
            const taskData = await TodoList.findByPk(id)
            const state = { state: true }
            if (!taskData)
                res.status(200).send({ msg: 'Task not found' })

            taskData.update(state)
            res.status(200).send(taskData)
        } catch (err) {

            res.status(400).send({ msg: `Bad Request` })
        }
    }




}


module.exports = todoList