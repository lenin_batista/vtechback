# VidaTech
## Backend



[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://gitlab.com/lenin_batista/vtechreact.git)

backend para TODO list de prueba tecnica de VidaTech

para inciar el backend :

- clone https://gitlab.com/lenin_batista/vtechreact.git
- run npm install
- crear base de datos en MySQl
- configurar conexion de base de datos en archivo bd/connection.js 
- run npm index

para crear un usuario puede usar postman con los siguientes datos
 url : localhost:8001/api/user/
 datos: {
    "firstName" : "name1",
    "lastName" : "lastName",
    "email" : "email@g.com",
    "passWord" : "123456789"
}