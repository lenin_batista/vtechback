'use strict'
// require('./lib/scheduleDeleteFiles')
const app = require('./app')
    //const createDb = require('./bd/db')
const sequelize = require('./bd/connection')
const port = 8001
app.listen(port, async() => {
    console.log(`el server esta corriendo en localhost: ${port}`)
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }

})