const express = require("express");
const cors = require('cors')
const app = express();

app.use(express.json())
app.use(cors())
const todoListRouter = require('./router/todoList')
const userRouter = require('./router/user')
app.use("/api/task/", todoListRouter)
app.use("/api/user/", userRouter)
module.exports = app